
void setLuminaria() {
  println_dbg("Configurando Luminária");
  
  channel = binToDecimal(pcf8574.readButton(PinBit1), pcf8574.readButton(PinBit2), pcf8574.readButton(PinBit4));
  println_dbg("Bit1");
  println_dbg(pcf8574.readButton(PinBit1));
  println_dbg("Bit2");
  println_dbg(pcf8574.readButton(PinBit2));
  println_dbg("Bit4");
  println_dbg(pcf8574.readButton(PinBit4));
  println_dbg("Canais");
  println_dbg(channel);
  if (channel == 1) {
  xLed = 55;
  espacoXLed = 60;
} else if (channel == 2) {
  xLed = 30;
  espacoXLed = 50;
} else if (channel == 3) {
  xLed = 10;
  espacoXLed = 45;
} else if (channel == 4) {
  xLed = 8;
  espacoXLed = 32;
} else if (channel == 5) {
  xLed = 6;
  espacoXLed = 25;
} else {
  xLed = 5;
  espacoXLed = 21;
}
}


void setTermometro() {
  println_dbg("Configurando o Termômetro (biblioteca Dallas Temperature)");
  // localiza o termômetro no barramento.
  print_dbg("Localizando termômetros...");
  sensors.begin();
  print_dbg("Encontrado(s): ");
  print_dbg(sensors.getDeviceCount(), DEC);
  println_dbg(" termômetro(s)");
  // report parasite power requirements
  print_dbg("Parasite power is: ");
  if (sensors.isParasitePowerMode()) println_dbg("ON");
  else println_dbg("OFF"); 
  if (!sensors.getAddress(insideThermometer, 0)) {
    println_dbg("Não foi possível achar o endereço para o termômetro 0");
  } else {
    println_dbg("Ttermômetro encontrado!");
    termometro = 0; 
    // set the resolution to 9 bit (Each Dallas/Maxim device is capable of several different resolutions)
    sensors.setResolution(insideThermometer, 9);
    print_dbg("Device 0 Resolution: ");
    print_dbg(sensors.getResolution(insideThermometer), DEC);
    println_dbg();
  }
}

void setFeeder() {
  println_dbg("Configurando Fedder");

}

void setTwinStar() {
  println_dbg("Configurando TwinStar");
  twinstar = pcf8574.readButton(PinTSDetect);
}

void setUV() {
println_dbg("Configurando UV");
UV = pcf8574.readButton(PinUVDetect);
}

void setSensorPH() {
println_dbg("Configurando SensorPH");
}

void setSensorNivel() {
  println_dbg("Configurando SensorNivel");

}
