////////VARS PRIMEIRAPASSAGEM/////////
boolean firstRun = true;
///////////////////////////////////
////////VARS pcf8574/////////
PCF8574 pcf8574(0x20);
///////////////////////////////////

////////VARS ESP8266Wifi.h/////////
char endIP[24];
char ssid[24];
///////////////////////////////////

////////VARS RESETPIN/////////
int buttonState = 1;
///////////////////////////////////

////////VARS OTA/////////
int buttonOTAState = 0;
boolean OTA_ENABLED = false;
///////////////////////////////////

////////VAR WEBSERVER/////////
ESP8266WebServer server(80);
////////////////////////////////////

////////VAR PCA8596/////////
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver(&Wire, 0x40);
////////////////////////////////////

////////VAR FIREBASE/////////
HTTPClient http;
String serve = "AAAAqRuRuTs:APA91bE0uls4mvjAmKUlL_jRpV9J1Kp53sUZBqPxur5muGWSUJcEM1aTX64swyc_niL7jCBXgVLs1R0IlpiC49KutfHHisBaoHcUrwT_MGt96CKnGjrXMshd31j5rY4GobuSgvsXAglh";
////////////////////////////////////

////////VAR OLED/////////
U8G2_SH1106_128X64_NONAME_F_HW_I2C u8g2(U8G2_R0, /* reset=*/ U8X8_PIN_NONE);
//U8G2_SSD1306_128X64_NONAME_F_HW_I2C u8g2(U8G2_R0, OLED_SCK, OLED_SDA/* reset=*/ U8X8_PIN_NONE);
//U8G2_SSD1306_128X64_NONAME_F_HW_I2C u8g2(U8G2_R0, /* reset=*/ U8X8_PIN_NONE);
Scheduler oledScreens = Scheduler();
int oledButtonState = 1;         // current state of the button
int oledLastButtonState = 1;     // previous state of the button
int oledPass = 0;     // previous state of the button
boolean oledSheduler = true;
int flag = 0;
////////////////////////////////////

////////VAR EMAIL/////////
//String  email="";
//boolean emailLigado=false;
////////////////////////////////////

////////VAR EMAIL_SMTP/////////
//String  smtp="";
//String  login="";
//String  password="";
//String  sender="";
//int smtpPort;
//boolean sslLigado=false;
////////////////////////////////////

////////VAR REGID/////////
String  regId = "";
boolean regIdLigado = false;
////////////////////////////////////

//////////VARS TERMOMETRO///////////
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
DeviceAddress insideThermometer;
float temperaturaAtual = 0;
float tempC = 0;
float tempMin = 0;
float tempMax = 0;
boolean alertaTemp = false;
////////////////////////////////////

////////VARS Float To String/////////
char charBuffer[25];
////////////////////////////////////

////////VAR Autorizacao/////////
String autorizacao;
////////////////////////////////////

////////VAR Modo Setup/////////
boolean modoSetup = false;
String presetSetup = "";
////////////////////////////////////

////////VARS Agendador/////////
Scheduler temporizadorAgendador = Scheduler();
Scheduler temporizadorPreset = Scheduler();
boolean temporizador = false;

struct Agendador {
  uint32_t hrInicio ;
  uint32_t hrFinal;
  String nome ;
  boolean gradual ;
};

Agendador agendador ;
Chronos::DateTime inicio;
////////////////////////////////////

////////VARS PING/////////
const char* remote_host = "8.8.8.8";
////////////////////////////////////

////////VARS TIMEPROVIDER/////////
int timeProvider; //0 = NTP   1 = RTC
int agendadorMudarProvider = 0; //0 = inativo   1 = ativo
Scheduler mudarProvider = Scheduler();
////////////////////////////////////

////////VARS NTP/////////
WiFiUDP Udp;
unsigned int localPort = 8888;  // local port to listen for UDP packets
const int NTP_PACKET_SIZE = 48; // NTP time is in the first 48 bytes of message
byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming & outgoing packets
// NTP Servers:
static const char ntpServerName[] = "pool.ntp.org";
int16_t utc = -3; //UTC -3:00 Brazil
////////////////////////////////////

////////VARS Preset/////////
struct Preset {
  String nome;
  long led1Tensao;
  long led2Tensao;
  long led3Tensao;
  long led4Tensao;
  long led5Tensao;
  long led6Tensao;
};

Preset preset1 = {"preset1", 0, 0, 0, 0, 0, 0};
Preset preset2 = {"preset2", 0, 0, 0, 0, 0, 0};
Preset preset3 = {"preset3", 0, 0, 0, 0, 0, 0};
Preset preset4 = {"preset4", 0, 0, 0, 0, 0, 0};
Preset preset5 = {"preset5", 0, 0, 0, 0, 0, 0};
Preset preset6 = {"preset6", 0, 0, 0, 0, 0, 0};
Preset preset7 = {"preset7", 0, 0, 0, 0, 0, 0};
Preset preset8 = {"preset8", 0, 0, 0, 0, 0, 0};
Preset preset9 = {"preset9", 0, 0, 0, 0, 0, 0};
Preset preset10 = {"preset10", 0, 0, 0, 0, 0, 0};
Preset preset11 = {"preset11", 0, 0, 0, 0, 0, 0};
Preset preset12 = {"preset12", 0, 0, 0, 0, 0, 0};
Preset presetManual = {"presetManual", 0, 0, 0, 0, 0, 0};
CreateHashMap(presetsHashMap, String, Preset, 13);
int presetsSize = 0;
int duracaoTotal = 0;
int duracaoAtual = 0;
int minuto = 60000;
////////////////////////////////////

////////VARS MetodoGradual/////////
boolean gradualCalculado = false;

float tensaoAtual[] = {0, 0, 0, 0, 0, 0};
float diferencaTensao[] = {0, 0, 0, 0, 0, 0};
float tensaoGradual[] = {0, 0, 0, 0, 0, 0};
////////////////////////////////////

////////VARS Storm////////////
Scheduler temporizadorStorm = Scheduler();
boolean hasStorm = false;
boolean hadStorm = false;
boolean hadFadeIn = false;
boolean hadFadeOut = false;

int BETWEEN_MIN = 3000;
int BETWEEN_MAX = 6000;
int VOLT_MIN = 100;
int VOLT_MAX = 255;
int DURATION = 43;
int TIMES_MIN = 2;
int TIMES_MAX = 7;
int stormMode;
int waitTime = 0;
////////////////////////////////////


////////VARS Power////////////
boolean power = true;
////////////////////////////////////

////////VARS Grafico/////////
int tensaoA = 0;
int tensaoB = 0;
int tensaoC = 0;
int tensaoD = 0;
int tensaoE = 0;
int tensaoF = 0;
int percentA = 0;
int percentB = 0;
int percentC = 0;
int percentD = 0;
int percentE = 0;
int percentF = 0;
/////////////////////////////

////////VARS ESTADO WIFI/////
int estadoWifi = 0;
int estadoWifiAtual = 0;
/////////////////////////////

///////COORDENADAS DESENHOS///////
/////TELA RELOGIO///////
int yRelogio = 45;
int yPresetNome = 62;
//////////////////
//////////TELA TEMP////////
int xTermometro = 96;
int yTermometro = 1;
int xTemp = 1;
int yTemp = 42;
int xCelsius = 68;
int yCelsius = 42;
int xCelsius2 = 74;
int yCelsius2 = 42;
//////////////////////
//////TELA WIFI////////
int xNome = 0;
int yNome = 10;
int xNomeValor = 20;
int yNomeValor = 10;
int xSSID = 0;
int ySSID = 22;
int xSSIDValor = 32;
int ySSIDValor = 22;
int xIP = 0;
int yIP = 34;
int xIPValor = 20;
int yIPValor = 34;
int xWifi = 50;
int yWifi = 42;
//////////////////////
/////////TELA PRESSET///////////////
int yLed = 13;
int tamXLed = 15;
int tamYLed = 0;
int xLed = 0;
int espacoXLed = 0;

////////////////////////////////////////////
/////////////////Light Off///////////////////////
int xCtrlLight = 29;
int yCtrlLight = 1;
/////////TELA EFEITO///////////////
int xStorm = 64;
int yStorm = 2;
int xCtrl = 36;
int yCtrl = 1;
int xCtrlStorm = 4;
int yCtrlStorm = 1;
int xCtrlRelogio = 46;
int yCtrlRelogio = 12;
int xCtrlRelogioStorm = 14;
int yCtrlRelogioStorm = 12;
int xPrst = 36;
int yPrst = 62;
int xPrstStorm = 5;
int yPrstStorm = 62;
int rPrst = 6;
int xCoil = 13;
int yCoil = 4;
int xAurora = 4;
int yAurora = 15;
////////////////////////////////////////////
//////////TELA DEVICES////////
int channel = 7;
int twinstar = 1;
int feeder = 1;
int termometro = 1;
int UV = 1;
int sensorPH = 1;
int sensorNivel = 1;

//////////////////////
//////////TWINSTAR////////
Scheduler temporizadorTwinStar = Scheduler();
boolean TSligado=false;
boolean TSativo=false;
uint32_t TSintervalo ;
uint32_t TSduracao;
long TSpotencia;
//////////////////////
