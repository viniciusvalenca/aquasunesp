////////////////////////Mensagens////////////////////////
void mensagemIniciarWLAN() {
  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_6x10_tf );
  println_dbg("\nVer 0.9");
  u8g2.drawStr(1, 10, "Ver 1.9");
  u8g2.sendBuffer();
  delay(1000);

  u8g2.clearBuffer();
  println_dbg();
  print_dbg("Conectando a: ");
  println_dbg(WiFi.SSID());
  u8g2.setFont(u8g2_font_6x10_tf );
  u8g2.drawStr(1, 10, "Conectando a rede");
  u8g2.drawStr(1, 20, "WLAN...");
  u8g2.drawXBMP(34, 22, WiFi_Logo_width, WiFi_Logo_height, WiFi_Logo_bits);
  println_dbg();
  u8g2.sendBuffer();
  delay(4000);
  sprintf(endIP, "%3d.%3d.%3d.%3d", WiFi.localIP()[0], WiFi.localIP()[1], WiFi.localIP()[2], WiFi.localIP()[3]);
  print_dbg("CHAR IP: ");
  println_dbg(endIP);

}

void mensagemLocalizandoDispositivos()
{
  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_6x10_tf );
  u8g2.drawStr(31, 10, "LOCALIZANDO");
  u8g2.drawStr(20, 20, "DISPOSITIVOS...");

  u8g2.sendBuffer();
}

void mensagemNenhumaRedeEncontrada()
{
  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_6x10_tf );

  u8g2.drawStr(43, 10, "NENHUMA");
  u8g2.drawStr(52, 20, "REDE");
  u8g2.drawStr(32, 30, "ENCONTRADA!");

  u8g2.sendBuffer();
}

void mensagemModoReconfiguracao()
{
  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_6x10_tf );
  u8g2.drawStr(43, 10, "MODO DE");
  u8g2.drawStr(22, 20, "RECONFIGURACAO");
  u8g2.drawStr(53, 30, "WIFI");
  u8g2.drawStr(41, 30, "ATIVADO!");

  u8g2.sendBuffer();
}

void mensageLocalizando() {
  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_6x10_tf );
  u8g2.drawStr(1, 10, "Localizando...");
  u8g2.sendBuffer();
}

void mensagemEnderecoIP() {
  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_6x10_tf );
  u8g2.drawStr(1, 10, "Endereco IP:");
  Serial.println(WiFi.localIP());
  u8g2.drawStr(1, 20, (char*) WiFi.localIP().toString().c_str());
  u8g2.sendBuffer();
}

void mensagemWifiConectado() {
  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_6x10_tf );
  u8g2.drawStr(22, 10, "WiFi conectado");
  u8g2.sendBuffer();
  //println_dbg("");
  //println_dbg("WiFi conectado");
}

void mensagemTentandoConectar() {
  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_6x10_tf );
  //Serial.println((128 - u8g2.getStrWidth("Tentando Conectar")) / 2);
  u8g2.drawStr(13, 10, "Tentando Conectar");
  u8g2.drawXBMP(34, 22, WiFi_Logo_width, WiFi_Logo_height, WiFi_Logo_bits);
  u8g2.sendBuffer();
}


//////////////////////FIM Mensagens//////////////////////
