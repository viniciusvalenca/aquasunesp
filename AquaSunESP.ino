#include <DS3232RTC.h>
#include <ESP8266WiFi.h>
#include <ESP8266Ping.h>
#include <ESP8266mDNS.h>
#include <ESP8266HTTPClient.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <U8g2lib.h>
#include <Wire.h>
#include <WiFiManager.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <HashMap.h>
#include <Scheduler.h>
#include <TimeLib.h>
#include <Chronos.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <math.h>
#include <ESP8266TrueRandom.h>
//#include <sendemail.h>
#include <Adafruit_PWMServoDriver.h>
#include "PCF8574.h"
#include "config.h"
#include "variaveis.h"
#include "functions.h"
#include "ota.h"
#include "images.h"
#include "displayOled.h"
#include "mensagens.h"
#include "devices.h"
#include "termometro.h"
#include "ntp.h"
#include "time.h"
#include "firebase.h"
#include "pca_9685.h"
#include "efeitos.h"

//for LED status
#include <Ticker.h>
Ticker ticker;
int state = 0;

void tick()
{
  println_dbg("tick");


  //toggle state
  state = !state;  // get the current state of GPIO1 pin
  if (state) {   
    digitalWrite(LED1, LOW);
    digitalWrite(LED2, LOW);
    digitalWrite(LED3, LOW);
  } else {
    digitalWrite(LED1, HIGH);
    digitalWrite(LED2, HIGH);
    digitalWrite(LED3, HIGH);    
  }
}





void configModeCallback (WiFiManager *myWiFiManager) {
  println_dbg("Entered config mode");
  println_dbg(WiFi.softAPIP());
  //if you used auto generated SSID, print it
  println_dbg(myWiFiManager->getConfigPortalSSID());
  setupOled();
  //setupPCA9685();
  mensagemNenhumaRedeEncontrada();
  delay(4000);
  mensagemModoReconfiguracao();
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  ticker.attach(0.2, tick);
}

void setup() {

  Serial.begin(115200);
  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wifiManager;
  wifiManager.setConfigPortalTimeout(180);
  WiFi.hostname(HOSTNAME_DEFAULT);
  //reset settings - for testing
  //wifiManager.resetSettings();

  //set callback that gets called when connecting to previous WiFi fails, and enters Access Point mode
  wifiManager.setAPCallback(configModeCallback);

  //fetches ssid and pass and tries to connect
  //if it does not connect it starts an access point with the specified name
  //here  "AutoConnectAP"
  //and goes into a blocking loop awaiting configuration
  if (!wifiManager.autoConnect(HOSTNAME_DEFAULT)) {
    println_dbg("failed to connect and hit timeout");
    //reset and try again, or maybe put it to deep sleep
    ESP.reset();
    delay(1000);
  }

  //if you get here you have connected to the WiFi
  println_dbg("conectado...");
  if (!MDNS.begin(HOSTNAME_DEFAULT)) {
    println_dbg("Error setting up MDNS responder!");
    while (1) {
      delay(1000);
    }
  }

  println_dbg("mDNS responder started");
  setupOTA();
  estadoWifi = 1;
  setupOled();
  mensagemIniciarWLAN();
  mensagemEnderecoIP();
  setupPCA9685();
  delay(4000);
  mensagemLocalizandoDispositivos();


  setLuminaria();
  setFeeder();
  setTwinStar();
  setUV();
  setSensorPH();
  setSensorNivel();
  setTermometro();
  setupPersistencia();


  // Add service to MDNS-SD
  MDNS.addService("coil-aurora", "tcp", 80);

  printf_dbg("\n\n");


  delay(2000);
  changeScreen();
}

/*########################LOOP#########################*/
void loop() {
 
  buttonState = pcf8574.readButton(RSTPIN);  
  //If button pressed...
  if (buttonState == LOW) {
    println_dbg("Reset acionado");
    resetDefaults();
  }


 oledButtonState = pcf8574.readButton(OLEDPIN);  
  if (oledButtonState != oledLastButtonState) {
    if (oledButtonState == LOW) {
      println_dbg("on");
      changeScreen();
      if (flag != 0) {
        println_dbg("Zerou");
        oledScreens = Scheduler();
        oledSheduler = false;
      } else {
        if (oledPass == 1) {
          println_dbg("Iniciou");
          oledSheduler = true;
          oledScreens = Scheduler();
          oledScreens.schedule(changeScreen, 10000);
          oledPass = 0;
        } else {
          println_dbg("Zerou");
          oledScreens = Scheduler();
          oledSheduler = false;
          oledPass = 1;
        }
      }
    }
  }
  oledLastButtonState = oledButtonState;


  //simbolo wifi
  if ( WiFi.status() == WL_CONNECTED ) {
    if ( estadoWifi == 2) {
    } else if ( estadoWifi == 0) {
      if (verificarInternet())
        estadoWifi = 1;
      else
        estadoWifi = 2;
    }
  } else {
    estadoWifi = 0;
  }


  if (timeProvider == 1 & estadoWifi == 2 & agendadorMudarProvider == 0) {
    mudarProvider.schedule(setupTime, 1800000);
    agendadorMudarProvider = 1;
  }

  if (TSligado) {
    //println_dbg("TSligado");
    temporizadorTwinStar.update();
  }


  if (!modoSetup) {
    if (temporizador == 1) {
      temporizadorAgendador.update();
      temporizadorPreset.update();
    }
    if (hasStorm || hadStorm) {
      if (!hadFadeIn) {
        fadeIn();
        hadFadeIn = true;
        temporizadorStorm.schedule(doStorm, ESP8266TrueRandom.random(BETWEEN_MIN, BETWEEN_MAX));

      }
      temporizadorStorm.update();

      if (!hadFadeOut && hadStorm) {
        fadeOut();
        hasStorm = false;
        hadFadeIn = false;
        hadStorm = false;
      }
    }
  }

  if (agendadorMudarProvider == 1) {
    mudarProvider.update();
  }

  if (termometro == 0) {
    sensors.requestTemperatures();
    verificarTemperatura(insideThermometer);//Variável tempC é modificada aqui...
    //Método para exibir gráficos dos leds com termômetro
    if (tempC > 0 && tempC != temperaturaAtual) {

      temperaturaAtual = tempC;
      if (temperaturaAtual < tempMin && !alertaTemp) {
        println_dbg("Alerta Temperatura Minima");
        println_dbg(temperaturaAtual);
        /*if(emailLigado==true){
          println_dbg("Alerta - Email");
          SendEmail e(smtp, smtpPort, login, password, 5000, sslLigado);
          e.send(sender, email, "nossl", "message");
          }*/
        if (regIdLigado == true) {
          println_dbg("Alerta - Push");
          doit("Alerta Temperatura Minima", String(temperaturaAtual));
        }
        alertaTemp = true;
      } else if (temperaturaAtual > tempMax && !alertaTemp) {
        println_dbg("Alerta Temperatura Maxima");
        println_dbg(temperaturaAtual);
        /*if(emailLigado==true){
          println_dbg("Alerta - Email");
           SendEmail e(smtp, smtpPort, login, password, 5000, sslLigado);
           e.send(sender, email, "nossl", "message");
          }*/
        if (regIdLigado == true) {
          println_dbg("Alerta - Push");
          doit("Alerta Temperatura Maxima", String(temperaturaAtual));
        }
        alertaTemp = true;
      } else if (temperaturaAtual <= tempMax && temperaturaAtual >= tempMin && alertaTemp) {
        alertaTemp = false;
      }
    } else if (tempC <= 0 && tempC != temperaturaAtual) {
      temperaturaAtual = tempC;
    }
  }
  server.handleClient();
  atualizarOled();


  if (!modoSetup) {
    oledScreens.update();
  }

}
/*########################OlED#########################*/


void atualizarOled() {

  if (modoSetup) {
    telaPreset();
  }
  else if (flag == 0) {
    telaPreset();
  } else if (flag == 1) {
    telaEfeito();
  }
  else if (flag == 2) {
    telaWifi();
  }
  else if (flag == 3) {
    telaRelogio();
  }
  else if (flag == 4) {
    telaTermometro();
  }

}

void changeScreen() {
  println_dbg("Trocou");
  if (!firstRun) {
    if (flag == 4) {
      flag = 0;
    } else if (flag == 3 && termometro == 1 ) {
      flag = 0;
    }  else if ( flag == 0 && oledPass == 1) {
      //oledPass = 0;
    } else {
      flag++;
    }
  } else {
    firstRun = false;
  }
  if (oledSheduler) {
    oledScreens.schedule(changeScreen, 10000);
  }
  if (!modoSetup) {
    if (flag == 0) {
      telaPreset();
    } else if (flag == 1) {
      telaEfeito();
    }
    else if (flag == 2) {
      telaWifi();
    }
    else if (flag == 3) {
      telaRelogio();
    }
    else if (flag == 4) {
      telaTermometro();
    }
  }
}

/*########################RESET#########################*/
void resetDefaults() {
  delay(3000);
  WiFi.disconnect();
  gravarTemporizador("{\"timer\":\"0\"}");
  gravarPower("{\"power\":\"1\"}");
  gravarStorm("{\"storm\":\"0\",\"mode\":\"1\"}");
  gravarUtc("{\"utc\":\"-3\"}");
  //gravarEmail("{\"email\":\"\",\"emailLigado\": \"0\"}");
  gravarRegId("{\"regId\":\"\",\"regIdLigado\": \"0\"}");
  gravarAutorizacao("{\"autorizacao\":\"0\"}");
  gravarTempAlerta("{\"tempMin\": 20,\"tempMax\": 30}");
  gravarPreset("{\"nome\":\"preset1\",\"led1Tensao\":900,\"led2Tensao\":300,\"led3Tensao\":500,\"led4Tensao\":900,\"led5Tensao\":300,\"led6Tensao\":500}");
  gravarPreset("{\"nome\":\"preset2\",\"led1Tensao\":500,\"led2Tensao\":500,\"led3Tensao\":1000,\"led4Tensao\":900,\"led5Tensao\":300,\"led6Tensao\":500}");
  gravarPreset("{\"nome\":\"preset3\",\"led1Tensao\":100,\"led2Tensao\":900,\"led3Tensao\":100,\"led4Tensao\":900,\"led5Tensao\":300,\"led6Tensao\":500}");
  gravarPreset("{\"nome\":\"preset4\",\"led1Tensao\":0,\"led2Tensao\":0,\"led3Tensao\":0,\"led4Tensao\":0,\"led5Tensao\":0,\"led6Tensao\":0}");
  gravarPreset("{\"nome\":\"preset5\",\"led1Tensao\":0,\"led2Tensao\":0,\"led3Tensao\":0,\"led4Tensao\":0,\"led5Tensao\":0,\"led6Tensao\":0}");
  gravarPreset("{\"nome\":\"preset6\",\"led1Tensao\":0,\"led2Tensao\":0,\"led3Tensao\":0,\"led4Tensao\":0,\"led5Tensao\":0,\"led6Tensao\":0}");
  gravarPreset("{\"nome\":\"preset7\",\"led1Tensao\":0,\"led2Tensao\":0,\"led3Tensao\":0,\"led4Tensao\":0,\"led5Tensao\":0,\"led6Tensao\":0}");
  gravarPreset("{\"nome\":\"preset8\",\"led1Tensao\":0,\"led2Tensao\":0,\"led3Tensao\":0,\"led4Tensao\":0,\"led5Tensao\":0,\"led6Tensao\":0}");
  gravarPreset("{\"nome\":\"preset9\",\"led1Tensao\":0,\"led2Tensao\":0,\"led3Tensao\":0,\"led4Tensao\":0,\"led5Tensao\":0,\"led6Tensao\":0}");
  gravarPreset("{\"nome\":\"preset10\",\"led1Tensao\":00,\"led2Tensao\":0,\"led3Tensao\":0,\"led4Tensao\":0,\"led5Tensao\":0,\"led6Tensao\":0}");
  gravarPreset("{\"nome\":\"preset11\",\"led1Tensao\":0,\"led2Tensao\":0,\"led3Tensao\":0,\"led4Tensao\":0,\"led5Tensao\":0,\"led6Tensao\":0}");
  gravarPreset("{\"nome\":\"preset12\",\"led1Tensao\":0,\"led2Tensao\":0,\"led3Tensao\":0,\"led4Tensao\":0,\"led5Tensao\":0,\"led6Tensao\":0}");
  gravarPreset("{\"nome\":\"presetManual\",\"led1Tensao\":100,\"led2Tensao\":300,\"led3Tensao\":500,\"led3Tensao\":0,\"led4Tensao\":0,\"led5Tensao\":0,\"led6Tensao\":0}");
  gravarAgendador("[{\"hrInicio\":0,\"hrFinal\":28800000,\"nome\":\"preset1\",\"gradual\":1},{\"hrInicio\":28800000,\"hrFinal\":57600000,\"nome\":\"preset2\",\"gradual\":1},{\"hrInicio\":57600000,\"hrFinal\":0,\"nome\":\"preset3\",\"gradual\":1}]");
  ESP.reset();
}
