uint16_t pulse_width_get;
uint16_t phase_shift_get;
void setupPCA9685() {
  pwm.begin();
  pwm.setPWMFreq(200);  // This is the maximum PWM frequency
}


void PCA9685AnalogWrite(int pwmnum, int power) {
   pwm.setPWM(pwmnum, 0, map(power,0,1024,0,4096));   
  
}
