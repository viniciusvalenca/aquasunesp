
#include "config.h"
#include "file.h"
#include <FS.h>



bool writeStringToFile(String path, String& dataString) {
  SPIFFS.remove(path);
  File file = SPIFFS.open(path, "w");
  if (!file) {
    print_dbg("File open Error: ");
    println_dbg(path);
    return false;
  }
  file.print(dataString);
  print_dbg("File Size: ");
  println_dbg(file.size(), DEC);
  file.close();
  print_dbg("Backup successful: ");
  println_dbg(path);
  print_dbg("data: ");
  println_dbg(dataString);
  return true;
}

bool getStringFromFile(String path, String& dataString) {
  File file = SPIFFS.open(path, "r");
  if (!file) {
    print_dbg("File open Error: ");
    println_dbg(path);
    return false;
  }
  print_dbg("File Size: ");
  println_dbg(file.size(), DEC);
  file.setTimeout(1);
  dataString = "";
  while (file.available()) {
    dataString += file.readString();
  }
  file.close();
  print_dbg("Restore successful: ");
  println_dbg(path);
  print_dbg("data: ");
  println_dbg(dataString);
  return true;
}

bool removeFile(String path) {
  println_dbg("Removed: " + path);
  return SPIFFS.remove(path);
}

