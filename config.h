/* Version */
#define AQUA_SUN      "v0.9"

/*
  DEFINE LEDS
*/
#define PinPCA9685_0        0
#define PinPCA9685_1        1
#define PinPCA9685_2        2
#define PinPCA9685_3        3
#define PinPCA9685_4        4
#define PinPCA9685_5        5
#define PinBit1             0
#define PinBit2             1
#define PinBit4             2


/*
  DEFINE TWINSTAR
*/
#define PinPCA9685_15        15
#define PinTSDetect          3
/*
/*
  DEFINE UV
*/
#define PinPCA9685_14        14
#define PinUVDetect          4
/*
  DEFINE TERMOMETRO
*/
#define ONE_WIRE_BUS D4
/*3
  DEFINE RESET PIN
*/
#define RSTPIN 7
#define OLEDPIN 6

#define LED1              D0
#define LED2              D8
#define LED3              D3



/*
  DEFINE OLED:
  SCK = GPIO04 -> D1
  SDA = GPIO05 -> D2
*/
#define OLED_SCK D2
#define OLED_SDA D1

/*Json buffer sizes*/
#define AGENDADOR_JSON_SIZE (JSON_ARRAY_SIZE(12) + 12*JSON_OBJECT_SIZE(4))
#define PRESET_JSON_SIZE (JSON_OBJECT_SIZE(4))
#define UTC_JSON_SIZE (JSON_OBJECT_SIZE(1))
#define EMAIL_JSON_SIZE (JSON_OBJECT_SIZE(2))
#define REGID_JSON_SIZE (JSON_OBJECT_SIZE(2))
#define AUTORIZACAO_JSON_SIZE (JSON_OBJECT_SIZE(1))
#define TEMPORIZADOR_JSON_SIZE (JSON_OBJECT_SIZE(1))
#define STORM_JSON_SIZE (JSON_OBJECT_SIZE(7))
#define TEMPALERTA_JSON_SIZE (JSON_OBJECT_SIZE(2))
#define TWINSTAR_JSON_SIZE (JSON_OBJECT_SIZE(4))

/* Hostname*/
#define HOSTNAME_DEFAULT        "CA4LM-120-19-0002"

/* for Debug */
#define SERIAL_DEBUG            true

#if SERIAL_DEBUG == true
#define DEBUG_SERIAL_STREAM     Serial
#define print_dbg               DEBUG_SERIAL_STREAM.print
#define printf_dbg              DEBUG_SERIAL_STREAM.printf
#define println_dbg             DEBUG_SERIAL_STREAM.println
#else
#define DEBUG_SERIAL_STREAM     NULL
#define print_dbg               // No Operation
#define printf_dbg              // No Operation
#define println_dbg             // No Operation
#endif
