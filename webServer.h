
void setAutorizacaoController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = "";
  if (gravarAutorizacao(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  println_dbg(message);

  //recarregar();
}

void getAutorizacaoController() {
  String json = lerAutorizacaoJson();
  server.send(200, "text/plain", json);
  println_dbg(json);
}


void setTempAlertaController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = "";
  if (gravarTempAlerta(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  println_dbg(message);

  //recarregar();
}

void getTempAlertaController() {
  String json = lerTempAlertaJson();
  server.send(200, "text/plain", json);
  println_dbg(json);
}

void setPresetController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = "";
  if (gravarPreset(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  println_dbg(message);
  if (!modoSetup) {
    recarregar();
  }
}

void getPresetController() {
  if (server.hasArg("preset") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String preset = server.arg("preset");
  String json = lerPresetJson(preset);
  server.send(200, "text/plain", json);
  println_dbg(json);
}

void setUtcController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = "";
  if ( gravarUtc(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  println_dbg(message);

  timeUpdate();
  recarregar();
}

void getUtcController() {
  String json = lerUtcJson();
  server.send(200, "text/plain", json);
  println_dbg(json);
}

void setAgendadorController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = "";
  if ( gravarAgendador(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  println_dbg(message);
  recarregar();
}

void getAgendadorController() {
  String json = lerAgendadorJson();
  server.send(200, "text/plain", json);
  println_dbg(json);
}


/*void setEmailController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = "";
  if ( gravarEmail(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  println_dbg(message);
  }

  void getEmailController() {
  String json = lerEmailJson();
  server.send(200, "text/plain", json);
  println_dbg(json);
  }*/

void setRegIdController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = "";
  if ( gravarRegId(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  println_dbg(message);
}

void getRegIdController() {
  String json = lerRegIdJson();
  server.send(200, "text/plain", json);
  println_dbg(json);
}

void setTimerController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = "";
  if ( gravarTemporizador(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  println_dbg(message);
  recarregar();
}

void getTimerController() {
  String json = lerTemporizadorJson();
  server.send(200, "text/plain", json);
  println_dbg(json);
}

void setStormController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = "";
  if ( gravarStorm(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  println_dbg(message);
  //recarregar();
}

void getStormController() {
  String json = lerStormJson();
  server.send(200, "text/plain", json);
  println_dbg(json);
}

void setPowerController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = "";
  if ( gravarPower(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  println_dbg(message);
  recarregar();
}

void getPowerController() {
  String json = lerPowerJson();
  server.send(200, "text/plain", json);
  println_dbg(json);
}


void setTwinStarController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
 println_dbg(server.arg("plain"));
  String message = "";
  if ( gravarTwinStar(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  println_dbg(message);
}

void getTwinStarController() {
  String json = lerTwinStarJson();
  server.send(200, "text/plain", json);
  println_dbg(json);
}

void getTemperaturaController() {
  String message = "";
  dtostrf(tempC, 4, 2, charBuffer);
  for (int i = 0; i < sizeof(charBuffer); i++)
  {
    message += (String)charBuffer[i];
  }
  message.replace(" ", "");
  server.send(200, "text / plain", (String)message);//Returns the HTTP response
}

void configuracaoController() {
  if (server.hasArg("setup") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  modoSetup = server.arg("setup").toInt();


  String message = "";
  if ( modoSetup ) {
    message = "true";
    presetSetup = server.arg("preset");
  } else {
    message = "false";
  }

  server.send(200, "text/plain", message);
  println_dbg(message);
  recarregar();
}

void resetController() {
  server.send(200, "text/plain", "true");
  println_dbg("true");
  resetDefaults();
}

void ledsController() {
  tensaoAtual[0] = server.arg("led1Tensao").toInt();
  tensaoAtual[1] = server.arg("led2Tensao").toInt();
  tensaoAtual[2] = server.arg("led3Tensao").toInt();
  tensaoAtual[3] = server.arg("led4Tensao").toInt();
  tensaoAtual[4] = server.arg("led5Tensao").toInt();
  tensaoAtual[5] = server.arg("led6Tensao").toInt();

  if (channel >= 1 & channel != 7)
    PCA9685AnalogWrite(PinPCA9685_0, tensaoAtual[0]);
  if (channel >= 2 & channel != 7)
    PCA9685AnalogWrite(PinPCA9685_1, tensaoAtual[1]);
  if (channel >= 3 & channel != 7)
    PCA9685AnalogWrite(PinPCA9685_2, tensaoAtual[2]);
  if (channel >= 4 & channel != 7)
    PCA9685AnalogWrite(PinPCA9685_3, tensaoAtual[3]);
  if (channel >= 5 & channel != 7)
    PCA9685AnalogWrite(PinPCA9685_4, tensaoAtual[4]);
  if (channel >= 6 & channel != 7)
    PCA9685AnalogWrite(PinPCA9685_5, tensaoAtual[5]);

  server.send(200, "text / plain", "true");        //Returns the HTTP response
}

void configController() {

  DynamicJsonBuffer jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();

  root["version"] = AQUA_SUN;
  root["hostname"] = HOSTNAME_DEFAULT;

  root["autorizacao"] = autorizacao;
  root["utc"] = utc;
  root["regId"] = regId;
  root["tempMin"] = tempMin;
  root["tempMin"] = tempMin;
  root["power"] = power;
  root["timer"] = temporizador;

  root["storm"] =  hasStorm;
  root["stormMode"] = stormMode;

  root["luminaria"] = channel;
  root["termometro"] = termometro;
  root["feeder"] = feeder;
  root["twinstar"] = twinstar;
  root["UV"] = UV;
  root["autorizacao"] = autorizacao;
  root["sensorPH"] = sensorPH;
  root["sensorNivel"] = sensorNivel;

  root["TSligado"] = TSligado;
  root["TSintervalo"] = TSintervalo;
  root["TSduracao"] = TSduracao;
  root["TSpotencia"] = TSpotencia;

  DynamicJsonBuffer jsonBuffer1 (AGENDADOR_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/sched.json", json)) {
    JsonArray& _root = jsonBuffer1.parseArray(json);
    presetsSize = _root.size();
    if (!_root.success()) {
      println_dbg("parseObject() failed");
      return;
    }

    root["agendador"] = _root;
    JsonArray & _presets = root.createNestedArray("presets");
    for (int i = 0; i < presetsSize; i++) {
      JsonObject& _preset = jsonBuffer.createObject();
      _preset["name"] = presetsHashMap.keyAt(i);
      lerPreset(presetsHashMap.keyAt(i));
      _preset["led1Tensao"] = presetsHashMap[presetsHashMap.keyAt(i)].led1Tensao;
      _preset["led2Tensao"] = presetsHashMap[presetsHashMap.keyAt(i)].led2Tensao;
      _preset["led3Tensao"] = presetsHashMap[presetsHashMap.keyAt(i)].led3Tensao;
      _preset["led4Tensao"] = presetsHashMap[presetsHashMap.keyAt(i)].led4Tensao;
      _preset["led5Tensao"] = presetsHashMap[presetsHashMap.keyAt(i)].led5Tensao;
      _preset["led6Tensao"] = presetsHashMap[presetsHashMap.keyAt(i)].led6Tensao;
      _presets.add(_preset);
    }
    JsonObject& _preset = jsonBuffer.createObject();
    _preset["name"] = "presetManual";
    lerPreset("presetManual");
    _preset["led1Tensao"] = presetsHashMap["presetManual"].led1Tensao;
    _preset["led2Tensao"] = presetsHashMap["presetManual"].led2Tensao;
    _preset["led3Tensao"] = presetsHashMap["presetManual"].led3Tensao;
    _preset["led4Tensao"] = presetsHashMap["presetManual"].led4Tensao;
    _preset["led5Tensao"] = presetsHashMap["presetManual"].led5Tensao;
    _preset["led6Tensao"] = presetsHashMap["presetManual"].led6Tensao;
    _presets.add(_preset);




    String resposta;
    root.printTo(resposta);

    server.send(200, "application/json", resposta);        //Returns the HTTP response
  }
}

void homeScreenController() {
  DynamicJsonBuffer jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  root["timer"] = temporizador;

  //  root["storm"] =  hasStorm;
  //  root["stormMode"] = stormMode;


  root["name"] = agendador.nome;
  root["hrFinal"] = agendador.hrFinal;
  root["led1Tensao"] = tensaoAtual[0];
  root["led2Tensao"] = tensaoAtual[1];
  root["led3Tensao"] = tensaoAtual[2];
  root["led4Tensao"] = tensaoAtual[3];
  root["led5Tensao"] = tensaoAtual[4];
  root["led6Tensao"] = tensaoAtual[5];


  String message = "";
  dtostrf(tempC, 4, 2, charBuffer);
  for (int i = 0; i < sizeof(charBuffer); i++)
  {
    message += (String)charBuffer[i];
  }
  message.replace(" ", "");

  root["temp"] = message;
  String resposta;
  root.printTo(resposta);

  server.send(200, "application/json", resposta);        //Returns the HTTP response

}

void setupWebServer() {
  println_dbg("Configurando WebServer!");

  //Iniciar o servidor
  server.on("/setAutorizacao", HTTP_POST, setAutorizacaoController);
  server.on("/getAutorizacao", HTTP_GET, getAutorizacaoController);
  server.on("/setTempAlerta", HTTP_POST, setTempAlertaController);
  server.on("/getTempAlerta", HTTP_GET, getTempAlertaController);
  server.on("/setPreset", HTTP_POST, setPresetController);
  server.on("/getPreset", HTTP_GET, getPresetController);
  server.on("/setAgendador", HTTP_POST, setAgendadorController);
  server.on("/getAgendador", HTTP_GET, getAgendadorController);
  //server.on("/setEmail", HTTP_POST, setEmailController);
  //server.on("/getEmail", HTTP_GET, getEmailController);
  server.on("/setRegId", HTTP_POST, setRegIdController);
  server.on("/getRegId", HTTP_GET, getRegIdController);
  server.on("/setUtc", HTTP_POST, setUtcController);
  server.on("/getUtc", HTTP_GET, getUtcController);
  server.on("/setTimer", HTTP_POST, setTimerController);
  server.on("/getTimer", HTTP_GET, getTimerController);
  server.on("/setStorm", HTTP_POST, setStormController);
  server.on("/getStorm", HTTP_GET, getStormController);
  server.on("/setPower", HTTP_POST, setPowerController);
  server.on("/getPower", HTTP_GET, getPowerController);
  server.on("/setTwinStar", HTTP_POST, setTwinStarController);
  server.on("/getTwinStar", HTTP_GET, getTwinStarController);
  server.on("/getTemperatura", HTTP_GET, getTemperaturaController);
  server.on("/modo", HTTP_GET, configuracaoController);
  server.on("/resetDefault", HTTP_GET, resetController);
  server.on("/leds", HTTP_GET, ledsController);
  server.on("/config", HTTP_GET, configController);
  server.on("/homeScreen", HTTP_GET, homeScreenController);
  server.begin();
}
