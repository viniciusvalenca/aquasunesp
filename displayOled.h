void setupOled() {
  println_dbg("Testando OLED! (SSD1306 LIB)");
  u8g2.begin();
  u8g2.clearBuffer();
  u8g2.drawXBMP(xCoil, yCoil, coil_width, coil_height, coil_bits);
  u8g2.sendBuffer();
  delay(3000);
  u8g2.clearBuffer();
  u8g2.drawXBMP(xAurora, yAurora, aurora_width, aurora_height, aurora_bits);
  u8g2.setFont(u8g2_font_6x10_tf);
  String myString=HOSTNAME_DEFAULT;
  String tmp = myString.substring(0, 12);
  u8g2.drawStr((128 - u8g2.getStrWidth(tmp.c_str())) / 2, 62, tmp.c_str());
  u8g2.sendBuffer();
  delay(3000);
}


String nomePreset(int idPreset) {
  String tmp;
  switch ( idPreset )
  {
    case 0 :
      tmp = "Ajuste 1";
      break;
    case 1 :
      tmp = "Ajuste 2";
      break;
    case 2 :
      tmp = "Ajuste 3";
      break;
    case 3 :
      tmp = "Ajuste 4";
      break;
    case 4 :
      tmp = "Ajuste 5";
      break;
    case 5 :
      tmp = "Ajuste 6";
      break;
    case 6 :
      tmp = "Ajuste 7";
      break;
    case 7 :
      tmp = "Ajuste 8";
      break;
    case 8 :
      tmp = "Ajuste 9";
      break;
    case 9 :
      tmp = "Ajuste 10";
      break;
    case 10 :
      tmp = "Ajuste 11";
      break;
    case 11 :
      tmp = "Ajuste 12";
      break;
    case 12 :
      tmp = "Manual";
      break;
  }
  return tmp;

}


void telaRelogio() {

  char bufferTmp [5];
  uint8_t minTmp, hourTmp;
  minTmp = minute();
  hourTmp = hour();
  sprintf(bufferTmp, "%02u:%02u\n", hourTmp, minTmp);
  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_logisoso42_tn);
  u8g2.drawStr((128 - u8g2.getStrWidth(bufferTmp)) / 2, yRelogio, bufferTmp );
  u8g2.setFont(u8g2_font_7x14_tf);
  u8g2.drawStr((128 - u8g2.getStrWidth(nomePreset(presetsHashMap.indexOf(agendador.nome)).c_str())) / 2, yPresetNome, nomePreset(presetsHashMap.indexOf(agendador.nome)).c_str());
  if (oledSheduler) {
    u8g2.setFont(u8g2_font_5x7_tf);
    u8g2.drawStr(124, 64, "A" );
  }
  u8g2.sendBuffer();

}

void telaTermometro() {
  u8g2.clearBuffer();
  u8g2.drawXBMP(xTermometro, yTermometro,  termometro_width, termometro_height, termometro_bits);
  float limite = 50;
  float valor = (tempC / limite) * 30;

  u8g2.setDrawColor(2);
  u8g2.drawBox(107, 37 - (int) valor, (termometro_width - 22), (int) valor + 2);

  u8g2.setDrawColor(1);
  char str_temp[4];
  dtostrf(tempC, 2, 1, str_temp);
  u8g2.setFont(u8g2_font_logisoso30_tf);
  u8g2.drawStr(xTemp, yTemp, str_temp);
  u8g2.drawStr(xCelsius, yCelsius, "\xb0");
  u8g2.drawStr(xCelsius2, yCelsius2, "C");
  if (oledSheduler) {
    u8g2.setFont(u8g2_font_5x7_tf);
    u8g2.drawStr(124, 64, "A" );
  }
  u8g2.sendBuffer();
}

void telaWifi() {
  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_6x10_tf );
  u8g2.drawStr(xNome, yNome, "ID: ");
  u8g2.drawStr(xNomeValor, yNomeValor, HOSTNAME_DEFAULT);
  u8g2.drawStr(xSSID, ySSID, "SSID: ");
  u8g2.drawStr(xSSIDValor, ySSIDValor, WiFi.SSID().c_str());
  u8g2.drawStr(xIP, yIP, "IP: ");
  u8g2.drawStr(xIPValor, yIPValor, (char*) WiFi.localIP().toString().c_str());

  if (estadoWifi == 1) {
    u8g2.drawXBMP(xWifi, yWifi, sinal_WIFI_width, sinal_WIFI_height, sinal_WIFI_bits);
  } else if (estadoWifi == 0) {
    u8g2.drawXBMP(xWifi, yWifi, no_sinal_WIFI_width, no_sinal_WIFI_height, no_sinal_WIFI_bits);
  } else if (estadoWifi == 2) {
    u8g2.drawXBMP(xWifi, yWifi,  no_Internet_width, no_Internet_height, no_Internet_bits);
  }
  estadoWifiAtual = estadoWifi;
  if (oledSheduler) {
    u8g2.setFont(u8g2_font_5x7_tf);
    u8g2.drawStr(124, 64, "A" );
  }
  u8g2.sendBuffer();
}


void telaPreset() {
  u8g2.clearBuffer();
  if(power){
  tensaoA =  modifiedMap(tensaoAtual[0], 0, 1024, 0, 50) + 0.5f;
  tensaoB = modifiedMap(tensaoAtual[1], 0, 1024, 0, 50) + 0.5f;
  tensaoC = modifiedMap(tensaoAtual[2], 0, 1024, 0, 50) + 0.5f;
  tensaoD = modifiedMap(tensaoAtual[3], 0, 1024, 0, 50) + 0.5f;
  tensaoE = modifiedMap(tensaoAtual[4], 0, 1024, 0, 50) + 0.5f;
  tensaoF = modifiedMap(tensaoAtual[5], 0, 1024, 0, 50) + 0.5f;


  percentA = modifiedMap(tensaoAtual[0], 0, 1024, 0, 100) + 0.5f;
  percentB = modifiedMap(tensaoAtual[1], 0, 1024, 0, 100) + 0.5f;
  percentC = modifiedMap(tensaoAtual[2], 0, 1024, 0, 100) + 0.5f;
  percentD = modifiedMap(tensaoAtual[3], 0, 1024, 0, 100) + 0.5f;
  percentE = modifiedMap(tensaoAtual[4], 0, 1024, 0, 100) + 0.5f;
  percentF = modifiedMap(tensaoAtual[5], 0, 1024, 0, 100) + 0.5f;

  u8g2.setFont(u8g2_font_6x13B_tf);
  u8g2.setFontDirection(0);
  u8g2.setFontMode(1);  /* activate transparent font mode */
  u8g2.setDrawColor(2);
  
  
if(channel >= 1 & channel != 7)
  u8g2.drawStr(xLed + 4, 11, "1");
if(channel >= 2 & channel != 7)
  u8g2.drawStr(xLed + espacoXLed + 4, 11, "2");
if(channel >= 3 & channel != 7)
  u8g2.drawStr(xLed + (espacoXLed  * 2) + 4, 11, "3");
if(channel >= 4 & channel != 7)
  u8g2.drawStr(xLed + (espacoXLed  * 3) + 4, 11, "4");
if(channel >= 5 & channel != 7)
  u8g2.drawStr(xLed + (espacoXLed  * 4) + 4, 11, "5");
if(channel >= 6 & channel != 7)
 u8g2.drawStr(xLed + (espacoXLed  * 5) + 4, 11, "6");
  

  u8g2.setFont(u8g2_font_6x13B_tf);
  u8g2.setFontDirection(3);
if(channel >= 1 & channel != 7){ 
  u8g2.drawStr(xLed + 12 , 60, (String(percentA) + "%").c_str());
  u8g2.drawBox(xLed, 63 - tensaoA, tamXLed, tensaoA);
}if(channel >= 2 & channel != 7){
  u8g2.drawStr(xLed + (espacoXLed + 12), 60, (String(percentB) + "%").c_str());
  u8g2.drawBox(xLed + espacoXLed, 63 - tensaoB, tamXLed, tensaoB);
}if(channel >= 3 & channel != 7){
  u8g2.drawStr(xLed + ((espacoXLed + 6) * 2), 60, (String(percentC) + "%").c_str());
  u8g2.drawBox(xLed + (espacoXLed * 2), 63 - tensaoC, tamXLed, tensaoC);
}if(channel >= 4 & channel != 7){
  u8g2.drawStr(xLed + ((espacoXLed * 3) + 12), 60, (String(percentD) + "%").c_str());
  u8g2.drawBox(xLed + (espacoXLed * 3), 63 - tensaoD, tamXLed, tensaoD);
}if(channel >= 5 & channel != 7){
  u8g2.drawStr(xLed + ((espacoXLed * 4) + 12), 60, (String(percentE) + "%").c_str());
  u8g2.drawBox(xLed + (espacoXLed * 4), 63 - tensaoE, tamXLed, tensaoE);
}if(channel >= 6 & channel != 7){
  u8g2.drawStr(xLed + ((espacoXLed * 5) + 12 ), 60, (String(percentF) + "%").c_str());
  u8g2.drawBox(xLed + (espacoXLed * 5), 63 - tensaoF, tamXLed, tensaoF);
}

 u8g2.setDrawColor(1);
if(channel >= 1 & channel != 7)
  u8g2.drawFrame(xLed, 13, tamXLed, 50);
if(channel >= 2 & channel != 7)
  u8g2.drawFrame(xLed + espacoXLed, 13, tamXLed, 50);
if(channel >= 3 & channel != 7)
   u8g2.drawFrame(xLed + (espacoXLed * 2), 13, tamXLed, 50);
if(channel >= 4 & channel != 7)
  u8g2.drawFrame(xLed + (espacoXLed * 3), 13, tamXLed, 50);
if(channel >= 5 & channel != 7) 
  u8g2.drawFrame(xLed + (espacoXLed * 4), 13, tamXLed, 50);
if(channel >= 6 & channel != 7)
 u8g2.drawFrame(xLed + (espacoXLed * 5), 13, tamXLed, 50);

  
  u8g2.setFontDirection(0);
  }else{
    u8g2.drawXBMP(xCtrlLight, yCtrlLight, lightoff_width, lightoff_height, lightoff_bits);
  }

  if (oledSheduler) {
    u8g2.setFont(u8g2_font_5x7_tf);
    u8g2.drawStr(124, 64, "A" );
  }
  u8g2.sendBuffer();
}

void telaEfeito() {
  u8g2.clearBuffer();
  if (temporizador) {
    /////////////////timer///////////////////
    if (hasStorm) {
      u8g2.drawXBMP(xCtrlStorm, yCtrlStorm, temporizado_width, temporizado_height, temporizado_bits);
      int clockRadius = 22.9;
      int limitador =  modifiedMap(duracaoTotal - duracaoAtual, 0, duracaoTotal, 0, 360) + 0.5f;
      for ( int z = 0; z <= limitador; z ++ ) {
        float angle = z ;
        angle = ( angle / 57.29577951 ) ; //Convert degrees to radians
        int x2 = ( xCtrlRelogioStorm + ( sin(angle) * clockRadius ) );
        int y2 = ( yCtrlRelogioStorm - ( cos(angle) * clockRadius ) );
        int x3 = ( xCtrlRelogioStorm + ( sin(angle) * ( clockRadius - ( clockRadius / 8 ) ) ) );
        int y3 = ( yCtrlRelogioStorm - ( cos(angle) * ( clockRadius - ( clockRadius / 8 ) ) ) );
        u8g2.drawLine( x2 + 14 , y2 + 14 , x3 + 14 , y3 + 14);
        u8g2.drawLine( x2 + 15 , y2 + 15 , x3 + 15 , y3 + 15);
        u8g2.drawLine( x2 + 16 , y2 + 16 , x3 + 16 , y3 + 16);
      }
      String strPreset =  nomePreset(presetsHashMap.indexOf(agendador.nome));
      u8g2.setFont(u8g2_font_7x14_tf);
      u8g2.drawStr(xPrstStorm , yPrstStorm, strPreset.c_str());
    } else {
      u8g2.drawXBMP(xCtrl, yCtrl, temporizado_width, temporizado_height, temporizado_bits);
      int clockRadius = 22.9;
      int limitador =  modifiedMap(duracaoTotal - duracaoAtual, 0, duracaoTotal, 0, 360) + 0.5f;
      for ( int z = 0; z <= limitador; z ++ ) {
        float angle = z ;
        angle = ( angle / 57.29577951 ) ; //Convert degrees to radians
        int x2 = ( xCtrlRelogio + ( sin(angle) * clockRadius ) );
        int y2 = ( yCtrlRelogio - ( cos(angle) * clockRadius ) );
        int x3 = ( xCtrlRelogio + ( sin(angle) * ( clockRadius - ( clockRadius / 8 ) ) ) );
        int y3 = ( yCtrlRelogio - ( cos(angle) * ( clockRadius - ( clockRadius / 8 ) ) ) );
        u8g2.drawLine( x2 + 14 , y2 + 14 , x3 + 14 , y3 + 14);
        u8g2.drawLine( x2 + 15 , y2 + 15 , x3 + 15 , y3 + 15);
        u8g2.drawLine( x2 + 16 , y2 + 16 , x3 + 16 , y3 + 16);
      }
      String strPreset =  nomePreset(presetsHashMap.indexOf(agendador.nome));
      u8g2.setFont(u8g2_font_7x14_tf);
      u8g2.drawStr(xPrst , yPrst, strPreset.c_str());
    }
  } else {
    ///////////////manual/////////////////
    if (hasStorm) {
      u8g2.drawXBMP(xCtrlStorm, yCtrlStorm, manual_width, manual_height, manual_bits);
      
      String strPreset =  nomePreset(presetsHashMap.indexOf(agendador.nome));
      u8g2.setFont(u8g2_font_7x14_tf);
      u8g2.drawStr(xPrstStorm , yPrstStorm, strPreset.c_str());
    } else {
      u8g2.drawXBMP(xCtrl, yCtrl, manual_width, manual_height, manual_bits);
      String strPreset =  nomePreset(presetsHashMap.indexOf(agendador.nome));
      u8g2.setFont(u8g2_font_7x14_tf);
      u8g2.drawStr(xPrst , yPrst, strPreset.c_str());
    }

  }

  /////////storm//////////////////
  if (hasStorm) {
    u8g2.drawXBMP(xStorm, yStorm, storm_MIN_width, storm_MIN_height, storm_MIN_bits);
  }
  ///////////////////////////////////
  if (oledSheduler) {
    u8g2.setFont(u8g2_font_5x7_tf);
    u8g2.drawStr(124, 64, "A" );
  }
  u8g2.sendBuffer();
}


