#include <SPI.h>
#include <FS.h>
#include <ArduinoJson.h>
#include "file.h"
#include "webServer.h"


void setupPersistencia() {
  print_dbg("Initializing FS...");
  // prepare SPIFFS
  SPIFFS.begin();

  presetsHashMap["preset1"] = preset1;
  presetsHashMap["preset2"] = preset2;
  presetsHashMap["preset3"] = preset3;
  presetsHashMap["preset4"] = preset4;
  presetsHashMap["preset5"] = preset5;
  presetsHashMap["preset6"] = preset6;
  presetsHashMap["preset7"] = preset7;
  presetsHashMap["preset8"] = preset8;
  presetsHashMap["preset9"] = preset9;
  presetsHashMap["preset10"] = preset10;
  presetsHashMap["preset11"] = preset11;
  presetsHashMap["preset12"] = preset12;
  presetsHashMap["presetManual"] = presetManual;

  println_dbg("Ler Config");

  lerAutorizacao();
  lerUtc();
  setupTime();
  digitalClockDisplay();

  lerTemporizador();
  lerTempAlerta();
  //lerEmail();
  lerRegId();
  lerPower();
  lerTwinStar();
  setupWebServer();
  recarregar();
}

void recarregar() {
  zerarTemporizadores();
  tensaoAtual[0] = 0;
  tensaoAtual[1] = 0;
  tensaoAtual[2] = 0;
  tensaoAtual[3] = 0;
  tensaoAtual[4] = 0;
  tensaoAtual[5] = 0;
  gradualCalculado = false;
  if (!modoSetup) {
    printf_dbg("temporizador ativo? %d\n", temporizador);
    if (temporizador) {
      lerAgendador();
    } else {
      agendador = {0, 0, "presetManual", 0};
      lerPreset(agendador.nome);
      aplicarPreset();
    }
    lerStorm();
  } else {
    println_dbg("Entrou no modo setup");
    lerPreset(presetSetup);
    tensaoAtual[0] = presetsHashMap[presetSetup].led1Tensao;
    tensaoAtual[1] = presetsHashMap[presetSetup].led2Tensao;
    tensaoAtual[2] = presetsHashMap[presetSetup].led3Tensao;
    tensaoAtual[3] = presetsHashMap[presetSetup].led4Tensao;
    tensaoAtual[4] = presetsHashMap[presetSetup].led5Tensao;
    tensaoAtual[5] = presetsHashMap[presetSetup].led6Tensao;

  if (channel >= 1 & channel != 7)
    PCA9685AnalogWrite(PinPCA9685_0, tensaoAtual[0]);
  if (channel >= 2 & channel != 7)
    PCA9685AnalogWrite(PinPCA9685_1, tensaoAtual[1]);
  if (channel >= 3 & channel != 7)
    PCA9685AnalogWrite(PinPCA9685_2, tensaoAtual[2]);
  if (channel >= 4 & channel != 7)
    PCA9685AnalogWrite(PinPCA9685_3, tensaoAtual[3]);
  if (channel >= 5 & channel != 7)
    PCA9685AnalogWrite(PinPCA9685_4, tensaoAtual[4]);
  if (channel >= 6 & channel != 7)
    PCA9685AnalogWrite(PinPCA9685_5, tensaoAtual[5]);
    
  }





}


//le o Autorizacao de dentro do sd e salva na variavel em memoria
void lerAutorizacao() {
  DynamicJsonBuffer jsonBuffer(AUTORIZACAO_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/autorizacao.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
      return;
    }
    autorizacao = root["autorizacao"].as<String>();;
    // Print values.
    println_dbg(autorizacao);
  } else {
    println_dbg("error opening autorizacao.json");
  }
  jsonBuffer.clear();
}

String lerAutorizacaoJson() {
  DynamicJsonBuffer jsonBuffer(AUTORIZACAO_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/autorizacao.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
    }
    println_dbg(autorizacao);
  } else {
    println_dbg("error opening autorizacao.json");
  }
  jsonBuffer.clear();
  return json;
}

//salva o Autorizacao no sd e na variavel em memoria
boolean gravarAutorizacao(String json) {
  DynamicJsonBuffer jsonBuffer(AUTORIZACAO_JSON_SIZE);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    println_dbg("parseObject() failed");
    return false;
  }
  autorizacao = root["autorizacao"].as<String>();;
  println_dbg(autorizacao);
  boolean resposta = writeStringToFile("/autorizacao.json", json);
  println_dbg(json);
  println_dbg("Writing done.");
  jsonBuffer.clear();
  return resposta;
}


//remove o Autorizacao no sd e na variavel em memoria
boolean removerAutorizacao() {
  autorizacao = "";
  boolean resposta = removeFile("/autorizacao.json");
  println_dbg("Remove done.");
  return resposta;
}


//le o utc de dentro do sd e salva na variavel em memoria
void lerUtc() {
  DynamicJsonBuffer jsonBuffer(UTC_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/utc.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
      return;
    }
    utc = root["utc"];
    // Print values.
    println_dbg(utc);
  } else {
    println_dbg("error opening utc.json");
  }
  jsonBuffer.clear();
}

String lerUtcJson() {
  DynamicJsonBuffer jsonBuffer(UTC_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/utc.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
    }
    println_dbg(utc);
  } else {
    println_dbg("error opening utc.json");
  }
  jsonBuffer.clear();
  return json;
}

//salva o utc no sd e na variavel em memoria
boolean gravarUtc(String json) {
  DynamicJsonBuffer jsonBuffer(UTC_JSON_SIZE);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    println_dbg("parseObject() failed");
    return false;
  }
  utc = root["utc"];
  println_dbg(utc);
  boolean resposta = writeStringToFile("/utc.json", json);

  println_dbg(json);
  println_dbg("Writing done.");
  jsonBuffer.clear();
  return resposta;
}

//le o email de dentro do sd e salva na variavel em memoria
/*void lerEmail() {
  DynamicJsonBuffer jsonBuffer(EMAIL_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/email.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
      return;
    }
    email = root["email"].asString();
    emailLigado = root["emailLigado"];
    // Print values.
    println_dbg(email);
  } else {
    println_dbg("error opening email.json");
  }
  jsonBuffer.clear();
  }

  String lerEmailJson() {
  DynamicJsonBuffer jsonBuffer(EMAIL_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/email.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
    }
    println_dbg(email);
  } else {
    println_dbg("error opening email.json");
  }
  jsonBuffer.clear();
  return json;
  }

  //salva o email no sd e na variavel em memoria
  boolean gravarEmail(String json) {
  DynamicJsonBuffer jsonBuffer(EMAIL_JSON_SIZE);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    println_dbg("parseObject() failed");
    return false;
  }
  regId = root["email"].asString();
  emailLigado = root["emailLigado"];
  println_dbg(regId);
  boolean resposta = writeStringToFile("/email.json", json);

  println_dbg(json);
  println_dbg("Writing done.");
  jsonBuffer.clear();
  return resposta;
  }*/

//le o TwinStar de dentro do sd e salva na variavel em memoria
void lerTwinStar() {
  DynamicJsonBuffer jsonBuffer(TWINSTAR_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/twinstar.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
      return;
    }
    TSligado = root["TSligado"];
    TSativo = !root["TSligado"];
    TSintervalo = root["TSintervalo"];
    TSduracao = root["TSduracao"];
    TSpotencia = root["TSpotencia"];
    // Print values.
    println_dbg(TSligado);
  println_dbg(TSintervalo);
  println_dbg(TSduracao);
  println_dbg(TSpotencia);
  } else {
    println_dbg("error opening twinstar.json");
  }
  jsonBuffer.clear();
}

String lerTwinStarJson() {
  DynamicJsonBuffer jsonBuffer(TWINSTAR_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/twinstar.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
    }
    println_dbg(autorizacao);
  } else {
    println_dbg("error opening twinstar.json");
  }
  jsonBuffer.clear();
  return json;
}

//salva o TwinStar no sd e na variavel em memoria
boolean gravarTwinStar(String json) {
  DynamicJsonBuffer jsonBuffer(TWINSTAR_JSON_SIZE);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    println_dbg("parseObject() failed");
    return false;
  }
  TSligado = root["TSligado"];
  TSintervalo = root["TSintervalo"];
  TSativo = !root["TSligado"];
  TSduracao = root["TSduracao"];
  TSpotencia = root["TSpotencia"];
  println_dbg(TSligado);
  println_dbg(TSintervalo);
  println_dbg(TSduracao);
  println_dbg(TSpotencia);
  boolean resposta = writeStringToFile("/twinstar.json", json);
  println_dbg(json);
  println_dbg("Writing done.");
  jsonBuffer.clear();
  TwinStar();
  return resposta;
}

//le o RegId de dentro do sd e salva na variavel em memoria
void lerRegId() {
  DynamicJsonBuffer jsonBuffer(REGID_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/regId.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
      return;
    }
    regId = root["regId"].asString();
    regIdLigado = root["regIdLigado"];
    // Print values.
    println_dbg(regId);
  } else {
    println_dbg("error opening regId.json");
  }
  jsonBuffer.clear();
}

String lerRegIdJson() {
  DynamicJsonBuffer jsonBuffer(REGID_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/regId.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
    }
    println_dbg(regId);
  } else {
    println_dbg("error opening regId.json");
  }
  jsonBuffer.clear();
  return json;
}

//salva o RegId no sd e na variavel em memoria
boolean gravarRegId(String json) {
  DynamicJsonBuffer jsonBuffer(REGID_JSON_SIZE);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    println_dbg("parseObject() failed");
    return false;
  }
  regId = root["regId"].asString();
  regIdLigado = root["regIdLigado"];
  println_dbg(utc);
  boolean resposta = writeStringToFile("/regId.json", json);

  println_dbg(json);
  println_dbg("Writing done.");
  jsonBuffer.clear();
  return resposta;
}

void lerTempAlerta() {
  DynamicJsonBuffer jsonBuffer(TEMPALERTA_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/tempAlerta.json", json)) {
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
      return;
    }

    tempMin = root["tempMin"];  //Implicit cast
    tempMax = root["tempMax"];  //Implicit cast
    println_dbg(tempMin);
    println_dbg(tempMax);

  } else {
    println_dbg("error opening tempAlerta.json");
  }
  jsonBuffer.clear();
}


String lerTempAlertaJson() {
  DynamicJsonBuffer jsonBuffer(TEMPALERTA_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/tempAlerta.json", json)) {
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
    }
  } else {
    println_dbg("error opening tempAlerta.json");
  }
  jsonBuffer.clear();
  return json;
}


boolean gravarTempAlerta(String json) {
  DynamicJsonBuffer jsonBuffer(TEMPALERTA_JSON_SIZE);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    println_dbg("parseObject() failed");
    return false;
  }

  tempMin = root["tempMin"];  //Implicit cast
  tempMax = root["tempMax"];  //Implicit cast
  println_dbg(tempMin);
  println_dbg(tempMax);

  println_dbg(json);
  boolean resposta = writeStringToFile("/tempAlerta.json", json);
  println_dbg("Writing done.");
  jsonBuffer.clear();
  return resposta;
}

//le a variavel temporizador e salva na variavel em memoria
void lerTemporizador() {
  DynamicJsonBuffer jsonBuffer(TEMPORIZADOR_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/timer.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
      return;
    }
    temporizador = root["timer"];
    // Print values.
    println_dbg(temporizador);
  } else {
    println_dbg("error opening timer.json");
  }
  jsonBuffer.clear();
}

String lerTemporizadorJson() {
  DynamicJsonBuffer jsonBuffer(TEMPORIZADOR_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/timer.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
    }
    // Print values.
    println_dbg(temporizador);
  } else {
    println_dbg("error opening timer.json");
  }
  jsonBuffer.clear();
  return json;
}

//salva o temporizador no sd e na variavel em memoria
boolean gravarTemporizador(String json) {
  DynamicJsonBuffer jsonBuffer(TEMPORIZADOR_JSON_SIZE);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    println_dbg("parseObject() failed");
    return false;
  }
  temporizador = root["timer"];
  println_dbg(temporizador);

  boolean resposta = writeStringToFile("/timer.json", json);

  println_dbg(json);
  println_dbg("Writing done.");
  jsonBuffer.clear();
}

//le a variavel storm e salva na variavel em memoria
void lerStorm() {
  DynamicJsonBuffer jsonBuffer(STORM_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/storm.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
      return;
    }
    hasStorm = root["storm"];
    setStormMode(root["stormMode"]);
    stormMode = root["stormMode"];
    // Print values.
    println_dbg(hasStorm);
  } else {
    println_dbg("error opening storm.json");
  }
  jsonBuffer.clear();
}

String lerStormJson() {
  DynamicJsonBuffer jsonBuffer(STORM_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/storm.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
    }
    // Print values.
    println_dbg(hasStorm);
  } else {
    println_dbg("error opening storm.json");
  }
  jsonBuffer.clear();
  return json;
}

//salva o storm no sd e na variavel em memoria
boolean gravarStorm(String json) {
  DynamicJsonBuffer jsonBuffer(STORM_JSON_SIZE);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    println_dbg("parseObject() failed");
    return false;
  }
  hasStorm = root["storm"];
  setStormMode(root["stormMode"]);
  stormMode = root["stormMode"];

  hadStorm = !hasStorm;
  println_dbg(hasStorm);

  boolean resposta = writeStringToFile("/storm.json", json);

  println_dbg(json);
  println_dbg("Writing done.");
  jsonBuffer.clear();
}

//le a variavel power e salva na variavel em memoria
void lerPower() {
  DynamicJsonBuffer jsonBuffer(STORM_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/power.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
      return;
    }
    power = root["power"];
    // Print values.
    println_dbg(power);
  } else {
    println_dbg("error opening power.json");
  }
  jsonBuffer.clear();
}

String lerPowerJson() {
  DynamicJsonBuffer jsonBuffer(STORM_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/power.json", json)) {
    println_dbg(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
    }
    // Print values.
    println_dbg(power);
  } else {
    println_dbg("error opening power.json");
  }
  jsonBuffer.clear();
  return json;
}

//salva o power no sd e na variavel em memoria
boolean gravarPower(String json) {
  DynamicJsonBuffer jsonBuffer(STORM_JSON_SIZE);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    println_dbg("parseObject() failed");
    return false;
  }
  power = root["power"];

  println_dbg(power);

  boolean resposta = writeStringToFile("/power.json", json);

  println_dbg(json);
  println_dbg("Writing done.");
  jsonBuffer.clear();
}

//le o preset
void lerPreset(String preset) {
  DynamicJsonBuffer jsonBuffer(PRESET_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/" + preset + ".json", json)) {
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
      return;
    }
    presetsHashMap[preset].led1Tensao = root["led1Tensao"];
    presetsHashMap[preset].led2Tensao = root["led2Tensao"];
    presetsHashMap[preset].led3Tensao = root["led3Tensao"];
    presetsHashMap[preset].led4Tensao = root["led4Tensao"];
    presetsHashMap[preset].led5Tensao = root["led5Tensao"];
    presetsHashMap[preset].led6Tensao = root["led6Tensao"];

    println_dbg(preset);
    println_dbg(json);
  } else {
    println_dbg("error opening " + preset + ".json");
  }
  jsonBuffer.clear();
}

//le o preset
String lerPresetJson(String preset) {
  DynamicJsonBuffer jsonBuffer(PRESET_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/" + preset + ".json", json)) {
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
    }
    println_dbg(preset);
    println_dbg(json);
  } else {
    println_dbg("error opening " + preset + ".json");
  }
  jsonBuffer.clear();
  return json;
}

//salvar preset
boolean gravarPreset(String json) {
  DynamicJsonBuffer jsonBuffer(PRESET_JSON_SIZE);
  println_dbg(json);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    println_dbg("parseObject() failed");
    return false;
  }
  String preset = root["nome"];

  presetsHashMap[preset].led1Tensao = root["led1Tensao"];
  presetsHashMap[preset].led2Tensao = root["led2Tensao"];
  presetsHashMap[preset].led3Tensao = root["led3Tensao"];
  presetsHashMap[preset].led4Tensao = root["led4Tensao"];
  presetsHashMap[preset].led5Tensao = root["led5Tensao"];
  presetsHashMap[preset].led6Tensao = root["led6Tensao"];

  //copiar metodo de cima retorno

  boolean resposta = writeStringToFile("/" + preset + ".json", json);

  println_dbg("Writing done.");
  jsonBuffer.clear();
  return resposta;
}


//le o agendador e verifica qual o preset deve ser chamado e aplica ele (o metodo de aplicar ainda n foi terminado)
void lerAgendador() {

  digitalClockDisplay();
  DynamicJsonBuffer jsonBuffer(AGENDADOR_JSON_SIZE);
  temporizadorPreset = Scheduler();
  String json;
  yield();
  if (getStringFromFile("/sched.json", json)) {
    JsonArray& root = jsonBuffer.parseArray(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
      return;
    }
    presetsSize = root.size();
    println_dbg("Tamanho vetor :");
    println_dbg(presetsSize);

    for (JsonObject& item : root) {
      uint32_t hrInicio = item["hrInicio"];  //Implicit cast
      uint32_t hrFinal = item["hrFinal"];  //Implicit cast
      String nome = item["nome"];  //Implicit cast
      boolean gradual = item["gradual"];  //Implicit cast
      uint32_t duracao = calcularDuracaoPreset(hrInicio, hrFinal);
      println_dbg(hrInicio);
      println_dbg(hrFinal);
      println_dbg(gradual);
      println_dbg(nome);
      println_dbg(duracao);

      println_dbg("teste de hora");
      setarHora(hrInicio, hrFinal);
      println_dbg();
      //inicio.printTo(Serial);
      int hours2 = setHoras(duracao);
      int minutes2 = setMinutos(duracao);
      int seconds2   = setSegundos(duracao);

      if (inicio <= Chronos::DateTime::now() && inicio + Chronos::Span::Hours(hours2) + Chronos::Span::Minutes(minutes2) + Chronos::Span::Seconds(seconds2) > Chronos::DateTime::now()) {
        agendador = {hrInicio, hrFinal, nome, gradual};
        print_dbg("Ativar o preset " + nome);
        printf_dbg(" gradual ativo? %d\n", agendador.gradual);
        lerPreset(agendador.nome);
        if (agendador.gradual) {
          gradualCalculado = false;
          aplicarPresetGradual();
        } else {
          duracaoTotal = calcularDuracaoPreset(agendador.hrInicio, agendador.hrFinal) / minuto;
          aplicarPreset();
        }
        print_dbg("AgendadorPreset para : ");
        println_dbg(calcularDuracaoPreset(horaEmMilissegundo(), hrFinal));
        temporizadorAgendador.schedule(lerAgendador, (calcularDuracaoPreset(horaEmMilissegundo(), hrFinal)));
        break;
      }
    }
  } else {
    println_dbg("error opening sched.json");
  }
  jsonBuffer.clear();
}


String lerAgendadorJson() {
  DynamicJsonBuffer jsonBuffer(AGENDADOR_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/sched.json", json)) {
    JsonArray& root = jsonBuffer.parseArray(json);
    if (!root.success()) {
      println_dbg("parseObject() failed");
    }
  } else {
    println_dbg("error opening sched.json");
  }
  jsonBuffer.clear();
  return json;
}

//salva o arquivo agendador, falta colocar a logica para salvar nas variaveis, a definir
boolean gravarAgendador(String json) {
  DynamicJsonBuffer jsonBuffer(AGENDADOR_JSON_SIZE);
  yield();
  JsonArray& root = jsonBuffer.parseArray(json);
  if (!root.success()) {
    println_dbg("parseObject() failed");
    return false;
  }
  for (JsonObject& item : root) {
    String hrInicio = item["hrInicio"];  //Implicit cast
    String hrFinal = item["hrFinal"];  //Implicit cast
    String nome = item["nome"];  //Implicit cast
    boolean gradual = item["gradual"];  //Implicit cast
    println_dbg(hrInicio);
    println_dbg(hrFinal);
    println_dbg(gradual);
    println_dbg(nome);
  }

  println_dbg(json);

  boolean resposta = writeStringToFile("/sched.json", json);

  println_dbg("Writing done.");
  jsonBuffer.clear();
  return resposta;
}


//aplicar preset
void aplicarPreset() {
  digitalClockDisplay();
  tensaoAtual[0] = presetsHashMap[agendador.nome].led1Tensao;
  tensaoAtual[1] = presetsHashMap[agendador.nome].led2Tensao;
  tensaoAtual[2] = presetsHashMap[agendador.nome].led3Tensao;
  tensaoAtual[3] = presetsHashMap[agendador.nome].led4Tensao;
  tensaoAtual[4] = presetsHashMap[agendador.nome].led5Tensao;
  tensaoAtual[5] = presetsHashMap[agendador.nome].led6Tensao;
  if (!hasStorm && power) {
    if (channel >= 1 & channel != 7)
      PCA9685AnalogWrite(PinPCA9685_0, tensaoAtual[0]);
    if (channel >= 2 & channel != 7)
      PCA9685AnalogWrite(PinPCA9685_1, tensaoAtual[1]);
    if (channel >= 3 & channel != 7)
      PCA9685AnalogWrite(PinPCA9685_2, tensaoAtual[2]);
    if (channel >= 4 & channel != 7)
      PCA9685AnalogWrite(PinPCA9685_3, tensaoAtual[3]);
    if (channel >= 5 & channel != 7)
      PCA9685AnalogWrite(PinPCA9685_4, tensaoAtual[4]);
    if (channel >= 6 & channel != 7)
      PCA9685AnalogWrite(PinPCA9685_5, tensaoAtual[5]);
  } else if (!power) {
    if (channel >= 1 & channel != 7)
      PCA9685AnalogWrite(PinPCA9685_0, 0);
    if (channel >= 2 & channel != 7)
      PCA9685AnalogWrite(PinPCA9685_1, 0);
    if (channel >= 3 & channel != 7)
      PCA9685AnalogWrite(PinPCA9685_2, 0);
    if (channel >= 4 & channel != 7)
      PCA9685AnalogWrite(PinPCA9685_3, 0);
    if (channel >= 5 & channel != 7)
      PCA9685AnalogWrite(PinPCA9685_4, 0);
    if (channel >= 6 & channel != 7)
      PCA9685AnalogWrite(PinPCA9685_5, 0);
  }
  if (temporizador) {
    println_dbg("Sem Gradual");
    duracaoAtual = calcularDuracaoPreset(horaEmMilissegundo(), agendador.hrFinal) / minuto;

    int segundos = 60 - second();
    print_dbg("Sem Gradual para ");
    println_dbg(segundos);
    if (segundos == 0)
      temporizadorPreset.schedule(aplicarPreset, minuto);
    else
      temporizadorPreset.schedule(aplicarPreset, segundos * 1000);
  } else {
    println_dbg("Manual");
  }

}

//aplicar preset gradual
void aplicarPresetGradual() {

  digitalClockDisplay();
  println_dbg("Gradual");
  duracaoAtual = calcularDuracaoPreset(horaEmMilissegundo(), agendador.hrFinal) / minuto;
  if (!gradualCalculado) {
    tensaoGradual[0] = presetsHashMap[agendador.nome].led1Tensao;
    tensaoGradual[1] = presetsHashMap[agendador.nome].led2Tensao;
    tensaoGradual[2] = presetsHashMap[agendador.nome].led3Tensao;
    tensaoGradual[3] = presetsHashMap[agendador.nome].led4Tensao;
    tensaoGradual[4] = presetsHashMap[agendador.nome].led5Tensao;
    tensaoGradual[5] = presetsHashMap[agendador.nome].led6Tensao;
    print_dbg("Tensao Atual: ");
    println_dbg(tensaoAtual[0]);
    //pergar o prox preset
    String proxPreset = proximoPresetAgendador(agendador.nome);
    lerPreset(proxPreset);
    print_dbg("Tensao Prox Preset: ");
    println_dbg(presetsHashMap[proxPreset].led1Tensao);
    diferencaTensao[0] = presetsHashMap[proxPreset].led1Tensao;
    diferencaTensao[1] = presetsHashMap[proxPreset].led2Tensao;
    diferencaTensao[2] = presetsHashMap[proxPreset].led3Tensao;
    diferencaTensao[3] = presetsHashMap[proxPreset].led4Tensao;
    diferencaTensao[4] = presetsHashMap[proxPreset].led5Tensao;
    diferencaTensao[5] = presetsHashMap[proxPreset].led6Tensao;
    print_dbg("Diferenca Tesao: ");
    println_dbg(diferencaTensao[0]);
    //verificar a duracao do preset em minutos
    duracaoTotal = calcularDuracaoPreset(agendador.hrInicio, agendador.hrFinal) / minuto;
    print_dbg("Duracao Total: ");
    println_dbg(duracaoTotal);
    gradualCalculado = true;
  }
  //aplica tensaoGradual no led
  tensaoAtual[0] =   modifiedMap(duracaoTotal - duracaoAtual, 0, duracaoTotal, tensaoGradual[0], diferencaTensao[0]) + 0.5f;
  tensaoAtual[1] =   modifiedMap(duracaoTotal - duracaoAtual, 0, duracaoTotal, tensaoGradual[1], diferencaTensao[1]) + 0.5f;
  tensaoAtual[2] =   modifiedMap(duracaoTotal - duracaoAtual, 0, duracaoTotal, tensaoGradual[2], diferencaTensao[2]) + 0.5f;
  tensaoAtual[3] =   modifiedMap(duracaoTotal - duracaoAtual, 0, duracaoTotal, tensaoGradual[3], diferencaTensao[3]) + 0.5f;
  tensaoAtual[4] =   modifiedMap(duracaoTotal - duracaoAtual, 0, duracaoTotal, tensaoGradual[4], diferencaTensao[4]) + 0.5f;
  tensaoAtual[5] =   modifiedMap(duracaoTotal - duracaoAtual, 0, duracaoTotal, tensaoGradual[5], diferencaTensao[5]) + 0.5f;
  if (!hasStorm && power) {
    if (channel >= 1 & channel != 7)
      PCA9685AnalogWrite(PinPCA9685_0, tensaoAtual[0]);
    if (channel >= 2 & channel != 7)
      PCA9685AnalogWrite(PinPCA9685_1, tensaoAtual[1]);
    if (channel >= 3 & channel != 7)
      PCA9685AnalogWrite(PinPCA9685_2, tensaoAtual[2]);
    if (channel >= 4 & channel != 7)
      PCA9685AnalogWrite(PinPCA9685_3, tensaoAtual[3]);
    if (channel >= 5 & channel != 7)
      PCA9685AnalogWrite(PinPCA9685_4, tensaoAtual[4]);
    if (channel >= 6 & channel != 7)
      PCA9685AnalogWrite(PinPCA9685_5, tensaoAtual[5]);
  } else if (!power) {
    if (channel >= 1 & channel != 7)
      PCA9685AnalogWrite(PinPCA9685_0, 0);
    if (channel >= 2 & channel != 7)
      PCA9685AnalogWrite(PinPCA9685_1, 0);
    if (channel >= 3 & channel != 7)
      PCA9685AnalogWrite(PinPCA9685_2, 0);
    if (channel >= 4 & channel != 7)
      PCA9685AnalogWrite(PinPCA9685_3, 0);
    if (channel >= 5 & channel != 7)
      PCA9685AnalogWrite(PinPCA9685_4, 0);
    if (channel >= 6 & channel != 7)
      PCA9685AnalogWrite(PinPCA9685_5, 0);
  }
  print_dbg("Duracao Restante: ");
  println_dbg(duracaoAtual);
  print_dbg("Posicao Atual: ");
  println_dbg((duracaoTotal - duracaoAtual));
  print_dbg("Tensao Atual: ");
  println_dbg(tensaoAtual[0]);



  int segundos = 60 - second();
  print_dbg("Gradual para ");
  println_dbg(segundos);
  if (segundos == 0)
    temporizadorPreset.schedule(aplicarPresetGradual, minuto);
  else
    temporizadorPreset.schedule(aplicarPresetGradual, segundos * 1000);
}


void TwinStar() {
  if (TSativo) {
    TSativo = false;
    print_dbg("Desativou TwinStar");
    PCA9685AnalogWrite(PinPCA9685_15, 0);    
    temporizadorTwinStar.schedule(TwinStar, TSintervalo);
  } else {
    TSativo = true;
    print_dbg("Ativou TwinStar");
    PCA9685AnalogWrite(PinPCA9685_15, TSpotencia);
    temporizadorTwinStar.schedule(TwinStar, TSduracao);
  }
}


void zerarTemporizadores() {
  temporizadorAgendador = Scheduler();
  temporizadorPreset = Scheduler();
}

String proximoPresetAgendador(String preset) {
  String retorno = "";
  int id = presetsHashMap.indexOf(preset);
  int idPM = presetsHashMap.indexOf("presetManual");
  println_dbg("ID");
  println_dbg(id);
  println_dbg(idPM);
  println_dbg("SIZE");
  println_dbg(presetsSize);
  if (id == presetsSize - 1) {
    println_dbg("proximo preset nao existe");
    println_dbg(id + 1);
    retorno = presetsHashMap.keyAt(0);
  } else if (id < presetsSize - 1) {
    retorno = presetsHashMap.keyAt(id + 1);
    println_dbg("retorno");
    println_dbg(retorno);
  }
  return retorno;

}
