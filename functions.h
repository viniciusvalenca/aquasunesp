double modifiedMap(double x, double in_min, double in_max, double out_min, double out_max)
{
  double temp = (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
  temp = (int) (4 * temp + .5);
  return (double) temp / 4;
}

int binToDecimal(int bit1,int bit2, int bit4){
  int tmp=0;
  if(bit1==0)
  tmp+=1;
  if(bit2==0)
  tmp+=2;
  if(bit4==0)
  tmp+=4;
  return tmp;
}
